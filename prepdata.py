#!/usr/bin/env python

import random
import os, os.path
import shutil
import glob

FOLDER = "flowers"
OUTPUT = "./data"
FLOWERS = ['daisy', 'rose', 'tulip', 'dandelion', 'sunflower']

train_deploy = {}
train_validate = {}

# Prepare folders: training, validation, test
#  Train/Deploy  80/20
#  Train/Test  80/20   

# Randomly choose 20% for Train/Deploy hold-back
# Randomly choose 20% of Train/Validate for validation, the rest for development 

def sample_put(class_name, label, sample, folder, seq):
    print "Put data", class_name, label, sample, folder

    # folder / class_name / sample#
    orig_path = os.path.join(FOLDER, class_name, sample)
    new_path = os.path.join(OUTPUT, folder, class_name, "%s.jpg" % seq)
    
    dirname = os.path.dirname(new_path)
    if not os.path.exists(dirname):
       os.makedirs(dirname)
    shutil.copy(orig_path,new_path)
    print new_path

def class_samples(name, path):
    samples = os.listdir(path)
    print name, len(samples)

    # Train/Deploy 
    class_name = name
    total_sample = len(samples)
    test_holdback = int(0.2 * total_sample)
    train_validate = int(0.2 * (total_sample - test_holdback))
    train_dev = total_sample - test_holdback - train_validate

    print "Train/Deploy Hold-back", test_holdback
    print "Training/Dev", train_dev
    print "Train/Validate Validation", train_validate

    random.shuffle(samples) # shaking

    holdback = samples[:test_holdback]
    train = samples[test_holdback:]

    dev = train[:train_dev]  
    validate = train[train_dev:]

    print "Buckets:", len(holdback),  len(dev), len(validate)
    
    for seq, i in enumerate(holdback):
        sample_put(class_name,class_name,i,"deploy",seq)
    for seq, i in enumerate(dev):
        sample_put(class_name,class_name,i,"train",seq)
    for seq, i in enumerate(validate):
        sample_put(class_name,class_name,i,"validate",seq)


# Process Raw Dataset
for c,d,f in os.walk(FOLDER):
    print d
    for clsname in d:
       clspath = os.path.join(c,clsname)
       class_samples(clsname, clspath)



